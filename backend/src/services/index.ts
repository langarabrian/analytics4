import { Application } from '../declarations';
import guests from './guests/guests.service';
// Don't remove this comment. It's needed to format import lines nicely.

export default function (app: Application): void {
  app.configure(guests);
}
