import React, { useState, useEffect, FormEvent } from 'react';
import { Paginated } from '@feathersjs/feathers';
import client from './feathers';

interface Guest {
  firstName: string,
  lastName: string
};

type GuestFunction = (g: Guest) => void;

let addGuestFuncs: GuestFunction[] = [];

function setUpNewGuestHandler( addGuestFunc: GuestFunction) {
    addGuestFuncs[0] = addGuestFunc;
}

const guestsService = client.service('guests');

guestsService.on( 'created', (newGuest: Guest) => {
  addGuestFuncs[0]( newGuest );
});

function Guests() {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [allGuests, setAllGuests] = useState<Array<Guest>>([]);

  const guestRows = allGuests.map( (guest: Guest ) =>
    <tr>
      <td>{guest.firstName}</td>
      <td>{guest.lastName}</td>
    </tr>
  );

  useEffect(() => {
    function addGuest( newGuest: Guest ) {
      setAllGuests( [...allGuests, newGuest] );
    }
    setUpNewGuestHandler( addGuest );
  });

  useEffect(() => {
    guestsService
    .find()
    .then( (guestPage: Paginated<Guest>) => setAllGuests( guestPage.data ));
  }, []);

  const handleSubmit = (e: FormEvent) => {
      e.preventDefault();
      const element = e.currentTarget as HTMLFormElement;
      if ( element.checkValidity() ) {
        element.classList.remove('was-validated');
        guestsService
        .create({ firstName, lastName })
        .then( (guest: Guest) => {
          setFirstName("");
          setLastName("");
        });
      } else {
        element.classList.add('was-validated');
      }
  }

  return (
  <div>
    <div className="py-5 text-center">
      <h2>Guest List</h2>
    </div>

    <div className="row g-5">
      <div className="col-md-5 col-lg-4 order-md-last">
        <table className="table">
          <thead>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            </thead>
          <tbody>
            {guestRows}
          </tbody>
        </table>
      </div>
      <div className="col-md-7 col-lg-8">
        <form className="needs-validation" noValidate onSubmit={handleSubmit}>
          <div className="row g-3">
            <div className="col-sm-6">
              <label htmlFor="firstName" className="form-label">First name</label>
              <input type="text"
                     className="form-control"
                     id="firstName"
                     placeholder=""
                     value={firstName}
                     onChange={e => setFirstName( e.target.value )}
                     required />
              <div className="invalid-feedback">
                Valid first name is required.
              </div>
            </div>

            <div className="col-sm-6">
              <label htmlFor="lastName" className="form-label">Last name</label>
              <input type="text"
                     className="form-control"
                     id="lastName"
                     placeholder=""
                     value={lastName}
                     onChange={e => setLastName( e.target.value )}
                     required />
              <div className="invalid-feedback">
                Valid last name is required.
              </div>
            </div>

          </div>

          <hr className="my-4" />

          <button className="w-100 btn btn-primary btn-lg" type="submit">Register Guest</button>
        </form>
      </div>
    </div>
  </div>
  );
}

export default Guests;
